# poinsoni

Data visualization and sonification software designed in Unity to load in a set of X,Y,Z,R data for points and present them in a way scientists can better understand their spacial relation in order to spot errors and anomalies with greater ease.